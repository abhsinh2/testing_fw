package com.abhsinh2.testng.core;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.abhsinh2.testng.AbstractTestCase;
import com.abhsinh2.testng.sample.SampleSpringClass1;

/**
 * 
 * @author abhsinh2
 *
 */
public class TestngServiceTest extends AbstractTestCase {
    static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM-HH-mm-ss");

    @Test
    public void test() throws IOException {
        Tests tests = new Tests();
        List<String> testSpring = new ArrayList<>();
        testSpring.add(SampleSpringClass1.beanName);
        tests.setBeans(testSpring);

        String date = dateFormat.format(new Date());
        String outputDirectory = System.getProperty("user.dir") + "/" + date;

        TestngService testngService = new TestngService(tests);
        testngService.run(outputDirectory);

        File file = new File(outputDirectory);
        Assert.assertTrue(file.exists());
        Assert.assertTrue(file.isDirectory());

        deleteDummyReportFolder(date);
    }
}
