package com.abhsinh2.testng;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.logging.Logger;

import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

/**
 * 
 * @author abhsinh2
 *
 */
@ContextConfiguration(locations = { "classpath:META-INF/spring/test-core-context.xml" })
public abstract class AbstractTestCase extends AbstractTestNGSpringContextTests {

    protected Logger logger;
    protected String configFilePath;

    @BeforeClass
    public final void beforeClass() throws Exception {
        Class<? extends AbstractTestCase> currTestClass = getClass();
        this.logger = TestLogger.getLogger(currTestClass);
        ReportUtil.updateLogReport(getClass());
        String message = "Started executing class " + this.getClass().getName();
        logger.info(message);

        configFilePath = System.getProperty("user.dir") + "/" + "src/test/resources/conf/config.json";
    }

    @BeforeTest
    public void beforeTest() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    @AfterMethod
    public void afterMethod() {

    }

    @AfterTest
    public void afterTest() {

    }

    protected void createDummyReportZipFile(String filename) throws IOException {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFile = new File(root, filename);
        if (!dummyZipFile.exists()) {
            dummyZipFile.createNewFile();
        }
    }

    protected void deleteDummyReportZipFile(String filename) {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFile = new File(root, filename);
        if (dummyZipFile.exists()) {
            dummyZipFile.delete();
        }
    }

    protected void createDummyReportFolder(String filename) throws IOException {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFolder = new File(root, filename);
        if (!dummyZipFolder.exists()) {
            dummyZipFolder.createNewFile();
        }
    }

    protected void deleteDummyReportFolder(String filename) throws IOException {
        File root = new File(System.getProperty("user.dir"));
        File dummyZipFolder = new File(root, filename);
        if (dummyZipFolder.exists()) {
            Path rootPath = Paths.get(dummyZipFolder.getPath());
            Files.walk(rootPath, FileVisitOption.FOLLOW_LINKS).sorted(Comparator.reverseOrder()).map(Path::toFile)
                    .peek(System.out::println).forEach(File::delete);
        }
    }
}