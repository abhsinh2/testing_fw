package com.abhsinh2.testng;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * 
 * @author abhsinh2
 *
 */
public class HtmlFormatter extends Formatter {

    private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");

    public HtmlFormatter(String title) {

    }

    @Override
    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);

        builder.append("<tr>\n");

        builder.append("<td>");
        builder.append(df.format(new Date(record.getMillis())));
        builder.append("</td>\n");

        builder.append("<td>");
        builder.append(record.getSourceClassName());
        builder.append("</td>\n");

        builder.append("<td>");
        builder.append(record.getSourceMethodName());
        builder.append("</td>\n");

        builder.append("<td>");
        builder.append(record.getLevel());
        builder.append("</td>\n");

        builder.append("<td>");
        builder.append(formatMessage(record));
        builder.append("</td>\n");

        builder.append("</tr>\n");
        return builder.toString();
    }

    @Override
    public String getHead(Handler h) {
        return "<!DOCTYPE html>\n<head>\n<style>\n" + "table { width: 100% }\n" + "th { font:bold 10pt Tahoma; }\n"
                + "td { font:normal 10pt Tahoma; }\n" + "h1 {font:normal 11pt Tahoma;}\n" + "</style>\n" + "</head>\n"
                + "<body>\n" + "<table border=\"0\" cellpadding=\"5\" cellspacing=\"3\">\n" + "<tr align=\"left\">\n"
                + "\t<th style=\"width:10%\">Time</th>\n" + "\t<th style=\"width:10%\">Class</th>\n"
                + "\t<th style=\"width:10%\">Method</th>\n" + "\t<th style=\"width:10%\">Loglevel</th>\n"
                + "\t<th style=\"width:60%\">Log Message</th>\n" + "</tr>\n";
    }

    @Override
    public String getTail(Handler h) {
        StringBuilder builder = new StringBuilder();
        builder.append("</table>\n");
        builder.append("</body>\n");
        builder.append("</html>");
        return builder.toString();
    }
}
