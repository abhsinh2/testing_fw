package com.abhsinh2.testng.sample;

import org.testng.annotations.Test;

/**
 * 
 * @author abhsinh2
 *
 */
public class SampleSpringClass1 {
    public static final String beanName = "sampleSpringClass1";

    @Test
    public void test1() {
        System.out.println(this.getClass().getSimpleName() + " test1");
    }
}
