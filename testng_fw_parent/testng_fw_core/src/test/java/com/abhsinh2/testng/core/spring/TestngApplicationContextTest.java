package com.abhsinh2.testng.core.spring;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.abhsinh2.testng.AbstractTestCase;
import com.abhsinh2.testng.core.spring.TestngApplicationContext;
import com.abhsinh2.testng.sample.SampleSpringClass1;
import com.abhsinh2.testng.sample.SampleSpringClass2;

/**
 * 
 * @author abhsinh2
 *
 */
public class TestngApplicationContextTest extends AbstractTestCase {
    @Test(expectedExceptions = { NoSuchBeanDefinitionException.class })
    public void getBean_EmptyBeanNames() {
        TestngApplicationContext.getBean("");
    }

    @Test(expectedExceptions = { NoSuchBeanDefinitionException.class })
    public void getBean_invalidBeanName() {
        TestngApplicationContext.getBean("test");
    }

    @Test
    public void getBean() {
        Assert.assertTrue(TestngApplicationContext.getBean("sampleSpringClass1") instanceof SampleSpringClass1);
        Assert.assertTrue(TestngApplicationContext.getBean("sampleSpringClass2") instanceof SampleSpringClass2);
    }
}
