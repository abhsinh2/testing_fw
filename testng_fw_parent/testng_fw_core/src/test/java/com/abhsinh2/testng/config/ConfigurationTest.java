package com.abhsinh2.testng.config;

import java.io.File;
import org.testng.annotations.Test;

import com.abhsinh2.testng.AbstractTestCase;

/**
 * 
 * @author abhsinh2
 *
 */
public class ConfigurationTest extends AbstractTestCase {
    static Configuration read;

    @Test
    public void testReadConfigFile() throws Exception {
        Configuration read = Configuration.getInstance();
        read.readFile(new File(configFilePath));
    }
}
