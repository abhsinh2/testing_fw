package com.abhsinh2.testng.config;

import java.io.File;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.abhsinh2.testng.AbstractTestCase;

/**
 * 
 * @author abhsinh2
 *
 */
public class ApplicationTest extends AbstractTestCase {

    @Test
    public void testInit() {
        System.setProperty(Application.SYSTEM_PROPERTY_REPORT_ROOT_DIRECTORY, System.getProperty("user.dir"));
        System.setProperty(Application.SYSTEM_PROPERTY_CONFIG_FILE, configFilePath);
        Application.init();
        File reportRootDirectory = Configuration.getInstance().getReportRootDirectory();
        Assert.assertNotNull(reportRootDirectory);
        Assert.assertTrue(reportRootDirectory.exists());
        Assert.assertTrue(reportRootDirectory.isDirectory());
    }

    @Test
    public void testInitConfigFile() {
        System.setProperty(Application.SYSTEM_PROPERTY_REPORT_ROOT_DIRECTORY, System.getProperty("user.dir"));
        Application.init(configFilePath);
        File reportRootDirectory = Configuration.getInstance().getReportRootDirectory();
        Assert.assertNotNull(reportRootDirectory);
        Assert.assertTrue(reportRootDirectory.exists());
        Assert.assertTrue(reportRootDirectory.isDirectory());
    }
}
