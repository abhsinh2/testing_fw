package com.abhsinh2.testng;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * 
 * @author abhsinh2
 *
 */
public class TestLogger {

    public static Logger getLogger(Class<?> testClass) throws IOException {
        Logger logger = Logger.getLogger(testClass.getName());

        // registerTxtFileHandler(testClass, logger);
        return logger;
    }

    // suppress the logging output to the console
    public static void removeConsoleHandler(Logger logger) {
        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
            rootLogger.removeHandler(handlers[0]);
        }
    }

    public static void registerTxtFileHandler(Class<?> testClass, Logger logger) throws IOException {
        FileHandler fileTxt = new FileHandler("Logging.txt");
        SimpleFormatter formatterTxt = new SimpleFormatter();
        fileTxt.setFormatter(formatterTxt);
        logger.addHandler(fileTxt);
    }

    public static void registerHtmlFileHandler(Class<?> testClass, Logger logger) throws IOException {
        FileHandler fileHTML = new FileHandler(testClass.getName() + ".html");
        Formatter formatterHTML = new HtmlFormatter("Log Title");
        fileHTML.setFormatter(formatterHTML);
        logger.addHandler(fileHTML);
    }
}
