package com.abhsinh2.testng.core.report;

import java.io.File;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.abhsinh2.testng.AbstractTestCase;
import com.abhsinh2.testng.config.Application;
import com.abhsinh2.testng.core.report.service.ReportFileService;

/**
 * 
 * @author abhsinh2
 *
 */
public class ReportFileServiceTest extends AbstractTestCase {
    @Autowired
    ReportFileService reportFileService;

    @BeforeTest
    @Override
    public void beforeTest() {
        super.beforeTest();

        configFilePath = System.getProperty("user.dir") + "/" + "src/test/resources/conf/config.json";

        System.setProperty(Application.SYSTEM_PROPERTY_REPORT_ROOT_DIRECTORY, System.getProperty("user.dir"));
        System.setProperty(Application.SYSTEM_PROPERTY_CONFIG_FILE, configFilePath);

        Application.init();
    }

    @BeforeMethod
    @Override
    public void beforeMethod() {
        super.beforeMethod();
    }

    @AfterMethod
    @Override
    public void afterMethod() {
        super.afterMethod();
    }

    @Test
    public void loadAsResource() throws IOException {
        createDummyReportZipFile("test.zip");
        Resource dummyResource = reportFileService.loadAsResource("test");
        Assert.assertNotNull(dummyResource);
        deleteDummyReportZipFile("test.zip");
    }

    @Test
    public void compress() throws IOException {
        createDummyReportFolder("test");
        reportFileService.compress("test");

        File root = new File(System.getProperty("user.dir"));
        File dummyZipFile = new File(root, "test.zip");
        Assert.assertTrue(dummyZipFile.exists());

        deleteDummyReportZipFile("test.zip");
        deleteDummyReportFolder("test");
    }

    @Test
    public void deleteReportFolder() throws IOException {
        createDummyReportFolder("test");
        reportFileService.deleteReportFolder("test");

        File root = new File(System.getProperty("user.dir"));
        File dummyZipFile = new File(root, "test");
        Assert.assertTrue(!dummyZipFile.exists());
    }

    @Test
    public void deleteReportZipFile() throws IOException {
        createDummyReportZipFile("test.zip");
        reportFileService.deleteReportZipFile("test");

        File root = new File(System.getProperty("user.dir"));
        File dummyZipFile = new File(root, "test.zip");
        Assert.assertTrue(!dummyZipFile.exists());
    }
}
