package com.abhsinh2.testng.core.spring;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.abhsinh2.testng.AbstractTestCase;
import com.abhsinh2.testng.core.spring.SpringContextTestngFactory;
import com.abhsinh2.testng.sample.SampleSpringClass1;
import com.abhsinh2.testng.sample.SampleSpringClass2;

/**
 * 
 * @author abhsinh2
 *
 */
public class SpringContextTestngFactoryTest extends AbstractTestCase {

    @Test(expectedExceptions = { NoSuchBeanDefinitionException.class })
    public void createInstances_EmptyBeanNames() {
        String commaSepartedBeanNames = "";
        SpringContextTestngFactory springContextTestngFactory = new SpringContextTestngFactory();
        Object[] objs = springContextTestngFactory.createInstances(commaSepartedBeanNames);

        Assert.assertEquals(objs.length, 0);
    }

    @Test(expectedExceptions = { NoSuchBeanDefinitionException.class })
    public void createInstances_invalidBeanName() {
        String commaSepartedBeanNames = "test";
        SpringContextTestngFactory springContextTestngFactory = new SpringContextTestngFactory();
        Object[] objs = springContextTestngFactory.createInstances(commaSepartedBeanNames);

        Assert.assertEquals(objs.length, 0);
    }

    @Test
    public void createInstances() {
        String commaSepartedBeanNames = "sampleSpringClass1,sampleSpringClass2";
        SpringContextTestngFactory springContextTestngFactory = new SpringContextTestngFactory();
        Object[] objs = springContextTestngFactory.createInstances(commaSepartedBeanNames);

        Assert.assertEquals(objs.length, 2);
        Assert.assertTrue(objs[0] instanceof SampleSpringClass1);
        Assert.assertTrue(objs[1] instanceof SampleSpringClass2);
    }

    @Test(expectedExceptions = { NoSuchBeanDefinitionException.class })
    public void createInstances_validAndInvalidBean() {
        String commaSepartedBeanNames = "test,sampleSpringClass2";
        SpringContextTestngFactory springContextTestngFactory = new SpringContextTestngFactory();
        Object[] objs = springContextTestngFactory.createInstances(commaSepartedBeanNames);

        Assert.assertEquals(objs.length, 1);
        Assert.assertTrue(objs[1] instanceof SampleSpringClass1);
    }
}
