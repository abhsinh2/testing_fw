package com.abhsinh2.testng.core;

import java.util.List;

/**
 * POJO to represent either spring bean and it's methods or class and it's
 * methods to be executed from Testng
 * 
 * 
 * { "name": "", "methods": ["", ""] }
 * 
 * @author abhsinh2
 *
 */
public class Test {
    private String name;
    private List<String> methods;

    public Test() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMethods() {
        return methods;
    }

    public void setMethods(List<String> methods) {
        this.methods = methods;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("Test");
        sb.append("[");
        sb.append("name=");
        sb.append(name);
        sb.append(",");
        sb.append("methods=");
        sb.append("[");
        if (methods != null) {
            for (String s : methods) {
                sb.append(s + ",");
            }
        }
        sb.append("]");
        sb.append("]");
        sb.append("]");
        return sb.toString();
    }
}
