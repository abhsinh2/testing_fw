package com.abhsinh2.testng.core.log;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.impl.SimpleLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testng Logger to log test execution states.
 * 
 * @author abhsinh2
 *
 */
public class TestngLogger {
    private static final Logger ROOT_LOGGER = LoggerFactory.getLogger("testng.service");

    private static List<ITestngLogMessageListener> listeners = new ArrayList<ITestngLogMessageListener>();

    public static void setListeners(List<ITestngLogMessageListener> listeners) {
        TestngLogger.listeners = listeners;
    }

    public static void addListener(ITestngLogMessageListener listener) {
        TestngLogger.listeners.add(listener);
    }

    public static void removeListener(ITestngLogMessageListener listener) {
        TestngLogger.listeners.remove(listener);
    }

    public static Logger getRootLogger() {
        return ROOT_LOGGER;
    }

    private static void notifyListeners(Logger logger, int level, String message) {
        notifyListeners(logger, level, message, null);
    }

    private static void notifyListeners(Logger logger, int level, String message, Throwable exception) {
        for (ITestngLogMessageListener listener : TestngLogger.listeners) {
            listener.processLogMessage(logger, level, message, exception);
        }
    }

    public static Logger getLogger(String className) {
        return LoggerFactory.getLogger(className);
    }

    public static void error(Logger logger, String message) {
        logger.error(message);
        notifyListeners(logger, SimpleLog.LOG_LEVEL_ERROR, message);
    }

    public static void error(String message) {
        ROOT_LOGGER.error(message);
        notifyListeners(ROOT_LOGGER, SimpleLog.LOG_LEVEL_ERROR, message);
    }

    public static void warn(Logger logger, String message) {
        logger.warn(message);
        notifyListeners(logger, SimpleLog.LOG_LEVEL_WARN, message);
    }

    public static void warn(String message) {
        ROOT_LOGGER.warn(message);
        notifyListeners(ROOT_LOGGER, SimpleLog.LOG_LEVEL_WARN, message);
    }

    public static void info(Logger logger, String message) {
        logger.info(message);
        notifyListeners(logger, SimpleLog.LOG_LEVEL_INFO, message);
    }

    public static void info(String message) {
        ROOT_LOGGER.info(message);
        notifyListeners(ROOT_LOGGER, SimpleLog.LOG_LEVEL_INFO, message);
    }

    public static void debug(Logger logger, String message) {
        logger.debug(message);
        notifyListeners(logger, SimpleLog.LOG_LEVEL_DEBUG, message);
    }

    public static void debug(String message) {
        ROOT_LOGGER.debug(message);
        notifyListeners(ROOT_LOGGER, SimpleLog.LOG_LEVEL_DEBUG, message);
    }

    public static void exception(Logger logger, Throwable e) {
        logger.error(e.getMessage(), e);
        notifyListeners(logger, SimpleLog.LOG_LEVEL_ERROR, e.getMessage(), e);
    }

    public static void exception(Throwable e) {
        ROOT_LOGGER.error(e.getMessage(), e);
        notifyListeners(ROOT_LOGGER, SimpleLog.LOG_LEVEL_ERROR, e.getMessage(), e);
    }

    public static void exception(Logger logger, String message, Throwable e) {
        logger.error(message, e);
        notifyListeners(logger, SimpleLog.LOG_LEVEL_ERROR, message, e);
    }

    public static void exception(String message, Throwable e) {
        ROOT_LOGGER.error(message, e);
        notifyListeners(ROOT_LOGGER, SimpleLog.LOG_LEVEL_ERROR, message, e);
    }
}
