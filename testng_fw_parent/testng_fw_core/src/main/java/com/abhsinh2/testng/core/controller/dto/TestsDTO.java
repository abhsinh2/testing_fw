package com.abhsinh2.testng.core.controller.dto;

import java.util.ArrayList;
import java.util.List;

import com.abhsinh2.testng.core.Test;
import com.abhsinh2.testng.core.Tests;

/**
 * 
 * @author abhsinh2
 *
 */
public class TestsDTO {
    private List<String> beans;
    private List<TestDTO> bean;
    private List<String> clazzes;
    private List<TestDTO> clazz;

    public TestsDTO() {

    }

    public List<String> getBeans() {
        return beans;
    }

    public void setBeans(List<String> beans) {
        this.beans = beans;
    }

    public List<TestDTO> getBean() {
        return bean;
    }

    public void setBean(List<TestDTO> bean) {
        this.bean = bean;
    }

    public List<String> getClazzes() {
        return clazzes;
    }

    public void setClazzes(List<String> clazzes) {
        this.clazzes = clazzes;
    }

    public List<TestDTO> getClazz() {
        return clazz;
    }

    public void setClazz(List<TestDTO> clazz) {
        this.clazz = clazz;
    }

    public Tests toModel() {
        Tests tests = new Tests();
        tests.setBeans(this.beans);
        tests.setClazzes(this.clazzes);

        if (this.bean != null) {
            List<Test> testList = new ArrayList<>();
            for (TestDTO dto : this.bean) {
                testList.add(dto.toModel());
            }
            tests.setBean(testList);
        }

        if (this.clazz != null) {
            List<Test> testList = new ArrayList<>();
            for (TestDTO dto : this.clazz) {
                testList.add(dto.toModel());
            }
            tests.setClazz(testList);
        }
        return tests;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("TestsDTO ");
        sb.append("[");
        sb.append("beans");
        sb.append("[");
        if (beans != null) {
            for (String s : beans) {
                sb.append(s + ",");
            }
        }
        sb.append("]");
        sb.append(",");

        sb.append("bean");
        sb.append("[");
        if (bean != null) {
            for (TestDTO s : bean) {
                sb.append(s.toString() + ",");
            }
        }
        sb.append("]");
        sb.append(",");

        sb.append("clazzes");
        sb.append("[");
        if (clazzes != null) {
            for (String s : clazzes) {
                sb.append(s + ",");
            }
        }
        sb.append("]");
        sb.append(",");

        sb.append("clazz");
        sb.append("[");
        if (clazz != null) {
            for (TestDTO s : clazz) {
                sb.append(s.toString() + ",");
            }
        }
        sb.append("]");
        sb.append("]");

        return sb.toString();
    }
}
