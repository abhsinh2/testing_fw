package com.abhsinh2.testng.core.report.service;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import com.abhsinh2.testng.config.Configuration;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Report Service
 * 
 * @author abhsinh2
 *
 */
@Service
public class ReportFileService implements IReportService {

    private static Configuration readConfig = Configuration.getInstance();

    public ReportFileService() {
    }

    public Resource loadAsResource(String filename) {
        try {
            // if zip exists then return it
            Path file = load(filename + ".zip");
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }

            // check if folder with given filename exists, if yes the compress
            // it and return as resource
            file = load(filename);
            resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return compress(filename);
            }

            throw new ReportFileNotFoundException("Could not read file: " + filename);
        } catch (MalformedURLException e) {
            throw new ReportFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    private Path load(String filename) {
        Path rootLocation = Paths.get(readConfig.getReportRootDirectory().getPath());
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource compress(String filename) {
        File file = new File(readConfig.getReportRootDirectory(), filename);
        File zipfile = new File(readConfig.getReportRootDirectory(), filename + ".zip");
        try {
            ReportDirectoryUtility.compress(file.getPath(), zipfile.getPath());
            return new UrlResource(zipfile.toURI());
        } catch (IOException e) {
            throw new ReportCompressionException("Could not compress file: " + filename, e);
        }
    }

    @Override
    public void deleteReportFolder(String filename) throws IOException {
        Path file = load(filename);
        Files.walkFileTree(file, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Override
    public void deleteReportZipFile(String filename) {
        File file = new File(readConfig.getReportRootDirectory(), filename + ".zip");
        if (file.isFile())
            file.delete();
    }

    @Override
    public boolean reportExists(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }
}
