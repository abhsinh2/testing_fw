package com.abhsinh2.testng.core.spring;

import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;

/**
 * Testng Factory to create object using spring beans.
 * 
 * @author abhsinh2
 *
 */
public class SpringContextTestngFactory {
    public static final String testBeanNames = "beanNames";

    @Factory
    @Parameters({ "beanNames" })
    public Object[] createInstances(String testBeanIds) {
        String[] beans = testBeanIds.split(",");
        Object[] result = new Object[beans.length];
        int index = 0;
        for (String bean : beans) {
            bean = bean.trim();
            Object obj = TestngApplicationContext.getBean(bean);

            if (obj != null)
                result[index++] = obj;
        }
        return result;
    }
}
