package com.abhsinh2.testng.config;

import java.io.File;
import java.io.FileReader;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

/**
 * Class to read json based configuration file. This singleton class reads the
 * config file and stores into objects to get access through out application.
 * 
 * @author abhsinh2
 *
 */
public class Configuration {
    private static Configuration instance;
    private JsonObject rootObj;
    private File reportRootDirectory;

    public static final String KEY_REPORT_ROOT_DIRECTORY = "reportRootDirectory";

    private Configuration() {

    }

    /**
     * Returns instance for this class.
     * 
     * @return
     */
    public static Configuration getInstance() {
        if (null == instance) {
            synchronized (Configuration.class) {
                instance = new Configuration();
            }
        }
        return instance;
    }

    /**
     * Reads configuration file. Should be called only once.
     * 
     * @param configFile
     * @throws Exception
     */
    public void readFile(File configFile) throws Exception {
        JsonReader jsonReader = new JsonReader(new FileReader(configFile));
        JsonParser jsonParser = new JsonParser();
        rootObj = jsonParser.parse(jsonReader).getAsJsonObject();

        readReportRootDirectory();
    }

    /**
     * Read report root directory
     */
    private void readReportRootDirectory() {
        if (reportRootDirectory == null) {
            if (rootObj.get(KEY_REPORT_ROOT_DIRECTORY) != null
                    && !(rootObj.get(KEY_REPORT_ROOT_DIRECTORY) instanceof JsonNull)
                    && !rootObj.get(KEY_REPORT_ROOT_DIRECTORY).getAsString().trim().isEmpty()) {
                reportRootDirectory = new File(rootObj.get(KEY_REPORT_ROOT_DIRECTORY).getAsString().trim());
                if (!reportRootDirectory.exists() || !reportRootDirectory.isDirectory()) {
                    throw new IllegalArgumentException(reportRootDirectory + " is not directory");
                }
            }
        }
    }

    /**
     * Returns Report root directory
     * 
     * @return
     */
    public File getReportRootDirectory() {
        return reportRootDirectory;
    }

    /**
     * Sets Report root directory
     * 
     * @param reportRootdirectory
     */
    public void setReportRootDirectory(File reportRootdirectory) {
        this.reportRootDirectory = reportRootdirectory;
    }
}
