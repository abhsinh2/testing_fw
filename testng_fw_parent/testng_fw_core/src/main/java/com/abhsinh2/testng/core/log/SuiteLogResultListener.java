package com.abhsinh2.testng.core.log;

import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * Listener to log suite run.
 * 
 * @author abhsinh2
 *
 */
public class SuiteLogResultListener implements ISuiteListener {

    public void onStart(ISuite suite) {
        TestngLogger.info("Starting test execution of suite: " + suite.getName());
    }

    public void onFinish(ISuite suite) {
        TestngLogger.info("Finishing test execution of suite: " + suite.getName());
    }
}
