package com.abhsinh2.testng.core.log;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Testng listener to log Test execution.
 * 
 * @author abhsinh2
 *
 */
public class TestngLogResultListener extends TestListenerAdapter {
    @Override
    public void onConfigurationFailure(ITestResult itr) {
        super.onConfigurationFailure(itr);
        TestngLogger.info("Test configuration failed: " + itr.getTestClass().getName() + "." + itr.getName());
    }

    @Override
    public void onConfigurationSkip(ITestResult itr) {
        super.onConfigurationSkip(itr);
        TestngLogger.info("Test configuration skipped: " + itr.getTestClass().getName() + "." + itr.getName());
    }

    @Override
    public void onConfigurationSuccess(ITestResult itr) {
        super.onConfigurationSuccess(itr);
        TestngLogger.info("Test configuration passed: " + itr.getTestClass().getName() + "." + itr.getName());
    }

    @Override
    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);
        TestngLogger.info("Finishing test execution of suite: " + testContext.getSuite().getName() + ", test : "
                + testContext.getName());
        TestngLogger.info("Total execution time (sec): "
                + (testContext.getEndDate().getTime() - testContext.getStartDate().getTime()) / 1000);
        TestngLogger.info("Total tests run: " + testContext.getAllTestMethods().length + ", Passed: "
                + testContext.getPassedTests().size() + ", Failed: " + testContext.getFailedTests().size()
                + ", Skipped: " + testContext.getSkippedTests().size());
    }

    @Override
    public void onStart(ITestContext testContext) {
        TestngLogger.info("Starting test execution of suite: " + testContext.getSuite().getName() + ", test : "
                + testContext.getName());
    }

    @Override
    public void onTestFailure(ITestResult testResult) {
        super.onTestFailure(testResult);
        TestngLogger.exception("Test " + testResult.getTestClass().getName() + "." + testResult.getName()
                + " failed with the following error", testResult.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult testResult) {
        super.onTestSkipped(testResult);
        TestngLogger.info("Test case skipped: " + testResult.getTestClass().getName() + "." + testResult.getName());
    }

    @Override
    public void onTestSuccess(ITestResult testResult) {
        super.onTestSuccess(testResult);
        TestngLogger.info("Test case passed: " + testResult.getTestClass().getName() + "." + testResult.getName());
    }

    @Override
    public void onTestStart(ITestResult testResult) {
        super.onTestStart(testResult);
        TestngLogger
                .info("Starting execution of test " + testResult.getTestClass().getName() + "." + testResult.getName());
        TestngLogger.info("Dataset Input Parameters");
        Object[] parameters = testResult.getParameters();
        if (parameters != null && parameters.length > 0) {
            for (Object parameter : parameters) {
                TestngLogger.info(parameter.toString());
            }
        }
    }
}
