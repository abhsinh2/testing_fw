package com.abhsinh2.testng.core.controller;

import com.abhsinh2.testng.core.TestngService;
import com.abhsinh2.testng.core.Tests;

/**
 * Thread to run Testng tests
 * 
 * @author abhsinh2
 *
 */
public class TestRunnable implements Runnable {

    private Tests testModel;
    private String outputDirectory;

    public TestRunnable(Tests testModel, String outputDirectory) {
        this.testModel = testModel;
        this.outputDirectory = outputDirectory;
    }

    @Override
    public void run() {
        TestngService testngService = new TestngService(testModel);
        testngService.run(this.outputDirectory);
    }

}
