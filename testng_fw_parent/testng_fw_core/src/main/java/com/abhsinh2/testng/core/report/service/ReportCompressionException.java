package com.abhsinh2.testng.core.report.service;

/**
 * 
 * @author abhsinh2
 *
 */
public class ReportCompressionException extends RuntimeException {

    private static final long serialVersionUID = -8963062680528838849L;

    public ReportCompressionException(String message) {
        super(message);
    }

    public ReportCompressionException(String message, Throwable cause) {
        super(message, cause);
    }
}
