package com.abhsinh2.testng.core.controller.dto;

import java.util.List;

import com.abhsinh2.testng.core.Test;

/**
 * 
 * @author abhsinh2
 *
 */
public class TestDTO {
    private String name;
    private List<String> methods;

    public TestDTO() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMethods() {
        return methods;
    }

    public void setMethods(List<String> methods) {
        this.methods = methods;
    }

    public Test toModel() {
        Test test = new Test();
        test.setName(getName());
        test.setMethods(getMethods());
        return test;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("TestDTO");
        sb.append("[");
        sb.append("name=");
        sb.append(name);
        sb.append(",");
        sb.append("methods=");
        sb.append("[");
        if (methods != null) {
            for (String s : methods) {
                sb.append(s + ",");
            }
        }
        sb.append("]");
        sb.append("]");
        sb.append("]");
        return sb.toString();
    }
}
