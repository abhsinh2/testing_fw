package com.abhsinh2.testng.core.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Spring ApplicationContextAware to create lookup beans.
 * 
 * @author abhsinh2
 *
 */
public class TestngApplicationContext implements ApplicationContextAware {
    private static ApplicationContext ctx;

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

    public static Object getBean(String beanName) {
        return ctx.getBean(beanName);
    }

    public static <T> T getBean(Class<T> requiredType) {
        return ctx.getBean(requiredType);
    }

    public static ApplicationContext getContext() {
        return ctx;
    }
}
