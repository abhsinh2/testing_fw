package com.abhsinh2.testng.config;

import java.io.File;

/**
 * Class to initialize this application using system properties or json based
 * configuration file.
 *
 * @author abhsinh2
 *
 */
public class Application {
    private static Configuration readConfiguration = Configuration.getInstance();
    public static final String SYSTEM_PROPERTY_CONFIG_FILE = "test.core.config.file";
    public static final String SYSTEM_PROPERTY_REPORT_ROOT_DIRECTORY = "test.core.report.directory";

    /**
     * initialize application using system property
     */
    public static void init() {
        try {
            String configFilePath = System.getProperty(SYSTEM_PROPERTY_CONFIG_FILE);
            init(configFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    /**
     * initialize application using given Json file
     *
     * @param configFile
     */
    public static void init(String configFilePath) {
        try {
            String reportRootDirectory = System.getProperty(SYSTEM_PROPERTY_REPORT_ROOT_DIRECTORY);

            if (reportRootDirectory != null) {
                readConfiguration.setReportRootDirectory(new File(reportRootDirectory));
            }

            if (configFilePath != null) {
                readConfiguration.readFile(new File(configFilePath));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
