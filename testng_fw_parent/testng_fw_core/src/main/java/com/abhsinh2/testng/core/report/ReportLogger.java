package com.abhsinh2.testng.core.report;

import org.testng.Reporter;

/**
 * Logger to include messages in Report File
 * 
 * @author abhsinh2
 *
 */
public class ReportLogger {
    public static void log(String message) {
        Reporter.log(message);
    }
}
