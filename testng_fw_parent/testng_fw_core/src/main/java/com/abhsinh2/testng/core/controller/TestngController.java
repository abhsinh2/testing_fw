package com.abhsinh2.testng.core.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.abhsinh2.testng.config.Application;
import com.abhsinh2.testng.config.Configuration;
import com.abhsinh2.testng.core.Tests;
import com.abhsinh2.testng.core.controller.dto.TestsDTO;
import com.abhsinh2.testng.core.report.service.IReportService;
import com.abhsinh2.testng.core.report.service.ReportFileNotFoundException;

/**
 * Rest Controller to run Test and download report.
 *
 * @author abhsinh2
 *
 */
@Controller
@RequestMapping("/ws/testng")
public class TestngController {

    @Autowired
    private IReportService reportService;

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM-HH-mm-ss");

    @PostConstruct
    public void init() {
        System.setProperty(Application.SYSTEM_PROPERTY_REPORT_ROOT_DIRECTORY, "/Users/abhsinh2");
        Application.init();
    }

    /**
     * Run the Test
     *
     * @param test
     * @return
     */
    @RequestMapping(value = "/run", method = RequestMethod.POST, consumes = { "application/json" })
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String run(@RequestBody TestsDTO test) {
        String date = dateFormat.format(new Date());
        String outputDirectory = Configuration.getInstance().getReportRootDirectory().getPath() + "/" + date;

        System.out.println(test);
        System.out.println(outputDirectory);

        Tests testModel = test.toModel();
        Thread th = new Thread(new TestRunnable(testModel, outputDirectory));
        th.start();

        return date;
    }

    /**
     * Retrieve status of Test
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String status(@RequestParam(value = "id") String id) {
        System.out.println(id);
        boolean reportExists = reportService.reportExists(id);
        if (reportExists) {
            return "completed";
        }
        return "inprogress";
    }

    /**
     * Download report as zip
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/report/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> serveReportFile(@PathVariable String id) {
        Resource file = reportService.loadAsResource(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @ExceptionHandler(ReportFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(ReportFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}
