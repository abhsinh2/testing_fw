package com.abhsinh2.testng.core;

import java.util.List;

/**
 * POJO to represent which spring bean and it's methods or class and it's
 * methods to be executed from Testng
 * 
 * { "beans": [""], "bean": [ { "name": "", "methods": [] } ], "clazzes": [""],
 * "clazz": [ { "name": "", "methods": [] } ] }
 *
 * @author abhsinh2
 *
 */
public class Tests {
    private List<String> beans;
    private List<Test> bean;
    private List<String> clazzes;
    private List<Test> clazz;

    public Tests() {

    }

    public List<String> getBeans() {
        return beans;
    }

    public void setBeans(List<String> beans) {
        this.beans = beans;
    }

    public List<Test> getBean() {
        return bean;
    }

    public void setBean(List<Test> bean) {
        this.bean = bean;
    }

    public List<String> getClazzes() {
        return clazzes;
    }

    public void setClazzes(List<String> clazzes) {
        this.clazzes = clazzes;
    }

    public List<Test> getClazz() {
        return clazz;
    }

    public void setClazz(List<Test> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Tests ");
        sb.append("[");
        sb.append("beans");
        sb.append("[");
        if (beans != null) {
            for (String s : beans) {
                sb.append(s + ",");
            }
        }
        sb.append("]");
        sb.append(",");

        sb.append("bean");
        sb.append("[");
        if (bean != null) {
            for (Test s : bean) {
                sb.append(s.toString() + ",");
            }
        }
        sb.append("]");
        sb.append(",");

        sb.append("clazzes");
        sb.append("[");
        if (clazzes != null) {
            for (String s : clazzes) {
                sb.append(s + ",");
            }
        }
        sb.append("]");
        sb.append(",");

        sb.append("clazz");
        sb.append("[");
        if (clazz != null) {
            for (Test s : clazz) {
                sb.append(s.toString() + ",");
            }
        }
        sb.append("]");
        sb.append("]");

        return sb.toString();
    }
}
