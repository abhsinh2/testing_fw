package com.abhsinh2.testng.core.report.service;

/**
 * 
 * @author abhsinh2
 *
 */
public class ReportFileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -8963062680528838849L;

    public ReportFileNotFoundException(String message) {
        super(message);
    }

    public ReportFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
