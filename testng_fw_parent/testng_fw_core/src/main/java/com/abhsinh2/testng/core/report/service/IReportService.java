package com.abhsinh2.testng.core.report.service;

import java.io.IOException;

import org.springframework.core.io.Resource;

/**
 * 
 * @author abhsinh2
 *
 */
public interface IReportService {
    /**
     * Returns Report zip file for given file name.
     * 
     * @param filename
     * @return
     */
    Resource loadAsResource(String filename);

    /**
     * Compress given folder.
     * 
     * @param filename
     * @return
     */
    Resource compress(String folder);

    /**
     * Deletes given report folder
     * 
     * @param filename
     */
    void deleteReportFolder(String filename) throws IOException;

    /**
     * Deletes given report zip file
     * 
     * @param filename
     */
    void deleteReportZipFile(String filename);

    boolean reportExists(String filename);
}
