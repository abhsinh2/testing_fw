package com.abhsinh2.testng.core;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.abhsinh2.testng.core.log.SuiteLogResultListener;
import com.abhsinh2.testng.core.log.TestngLogResultListener;
import com.abhsinh2.testng.core.report.Reporter;
import com.abhsinh2.testng.core.spring.SpringContextTestngFactory;

/**
 * Class to run TestNG tests for given spring beans or classes.
 * 
 * @author abhsinh2
 *
 */
public class TestngService {
    private Tests testDTO;

    public TestngService(Tests testDTO) {
        this.testDTO = testDTO;
    }

    public void run(String outputDir) {
        TestNG testNG = createTestNG();
        testNG.setOutputDirectory(outputDir);
        testNG.run();
    }

    private TestNG createTestNG() {
        TestNG testNG = new TestNG();

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(createSuite());

        testNG.setXmlSuites(suites);

        testNG.addListener(new TestngLogResultListener());
        testNG.addListener(new SuiteLogResultListener());
        testNG.addListener(new Reporter());

        return testNG;
    }

    public XmlSuite createSuite() {
        XmlSuite suite = new XmlSuite();
        suite.setName("Testng Suite");

        List<XmlTest> tests = new ArrayList<XmlTest>();

        if (testDTO.getBeans() != null && testDTO.getBeans().size() > 0) {
            tests.add(createSpringBeansTest(suite, testDTO.getBeans()));
        }

        if (testDTO.getBean() != null && testDTO.getBean().size() > 0) {
            tests.addAll(createSpringBeanTest(suite, testDTO.getBean()));
        }

        suite.setTests(tests);

        return suite;
    }

    public XmlTest createSpringBeansTest(XmlSuite suite, List<String> beanNames) {
        String commaSeparatedBeanNames = String.join(",", beanNames);

        System.out.println("commaSeparatedBeanNames:" + commaSeparatedBeanNames);

        XmlTest test = new XmlTest(suite);
        test.setName("Spring Bean Test");
        test.addParameter(SpringContextTestngFactory.testBeanNames, commaSeparatedBeanNames);

        List<XmlClass> classes = new ArrayList<XmlClass>();
        XmlClass testClass = new XmlClass(SpringContextTestngFactory.class.getName());
        classes.add(testClass);

        test.setXmlClasses(classes);

        return test;
    }

    public List<XmlTest> createSpringBeanTest(XmlSuite suite, List<Test> beans) {
        List<XmlTest> list = new ArrayList<XmlTest>();

        for (Test bean : beans) {
            XmlTest test = new XmlTest(suite);
            test.setName("Spring Bean Test " + bean.getName());

            List<XmlClass> classes = new ArrayList<XmlClass>();
            XmlClass testClass = new XmlClass(SpringContextTestngFactory.class.getName());
            test.addParameter(SpringContextTestngFactory.testBeanNames, bean.getName());

            List<String> excludedMethods = new ArrayList<String>();
            excludedMethods.add("*.*");
            testClass.setExcludedMethods(excludedMethods);

            List<XmlInclude> includedMethods = new ArrayList<XmlInclude>();
            for (String methodName : bean.getMethods()) {
                includedMethods.add(new XmlInclude(methodName));
            }
            testClass.setIncludedMethods(includedMethods);

            classes.add(testClass);

            test.setXmlClasses(classes);

            list.add(test);
        }

        return list;
    }

    public XmlTest createClassesTest(XmlSuite suite, String[] classes) {
        XmlTest test = new XmlTest(suite);
        test.setName("Classes Test");

        List<XmlClass> testClasses = new ArrayList<XmlClass>();
        for (String clazz : classes) {
            XmlClass testClass = new XmlClass(clazz);
            testClasses.add(testClass);
        }

        test.setXmlClasses(testClasses);
        return test;
    }

    public XmlTest createClassesTest(XmlSuite suite, Test[] beans) {
        XmlTest test = new XmlTest(suite);
        test.setName("Class Test");
        List<XmlClass> classes = new ArrayList<XmlClass>();

        for (Test bean : beans) {
            XmlClass testClass = new XmlClass(bean.getName());

            List<String> excludedMethods = new ArrayList<String>();
            excludedMethods.add("*.*");
            testClass.setExcludedMethods(excludedMethods);

            List<XmlInclude> includedMethods = new ArrayList<XmlInclude>();
            for (String methodName : bean.getMethods()) {
                includedMethods.add(new XmlInclude(methodName));
            }
            testClass.setIncludedMethods(includedMethods);

            classes.add(testClass);
        }

        test.setXmlClasses(classes);
        return test;
    }
}
