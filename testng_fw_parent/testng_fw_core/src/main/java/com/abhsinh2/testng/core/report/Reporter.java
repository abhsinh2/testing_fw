package com.abhsinh2.testng.core.report;

import java.util.List;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;

/**
 * Testng Report to generate HTML/PDF. Not using currently.
 * 
 * @author abhsinh2
 *
 */
public class Reporter implements IReporter {
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
        System.out.println("outputDirectory:" + outputDirectory);
    }
}
