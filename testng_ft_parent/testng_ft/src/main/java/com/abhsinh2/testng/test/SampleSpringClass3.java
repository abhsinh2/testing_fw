package com.abhsinh2.testng.test;

import org.testng.annotations.Test;

/**
 * 
 * @author abhsinh2
 *
 */
public class SampleSpringClass3 {
    public static final String beanName = "sampleSpringClass3";

    @Test
    public void test1() {
        System.out.println(this.getClass().getSimpleName() + " test1");
    }
}
