package com.abhsinh2.testng.test;

import org.testng.annotations.Test;

/**
 * 
 * @author abhsinh2
 *
 */
public class SampleSpringClass4 {
    public static final String beanName = "sampleSpringClass4";

    @Test
    public void test1() {
        System.out.println(this.getClass().getSimpleName() + " test1");
    }
}
