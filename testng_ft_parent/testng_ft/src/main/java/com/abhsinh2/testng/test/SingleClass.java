package com.abhsinh2.testng.test;

import org.testng.annotations.Test;

/**
 * 
 * @author abhsinh2
 *
 */
public class SingleClass {
    @Test
    public void test1() {
        System.out.println(this.getClass().getSimpleName() + " test1");
    }
}
