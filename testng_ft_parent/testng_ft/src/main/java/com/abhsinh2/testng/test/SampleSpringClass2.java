package com.abhsinh2.testng.test;

import org.springframework.stereotype.Component;
import org.testng.annotations.Test;

/**
 * 
 * @author abhsinh2
 *
 */
@Component
public class SampleSpringClass2 {
    public static final String beanName = "sampleSpringClass2";

    @Test
    public void test1() {
        System.out.println(this.getClass().getSimpleName() + " test1");
    }
}
