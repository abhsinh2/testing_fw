package com.abhsinh2.testng.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.abhsinh2.testng.AbstractTest;
import com.abhsinh2.testng.core.report.service.IReportService;

//@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class TestngControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private IReportService reportService;

    public void should404WhenMissingFile() throws Exception {
        // given(this.reportService.loadAsResource("test.txt")).willThrow(ReportFileNotFoundException.class);
        // this.mvc.perform(get("/files/test.txt")).andExpect(status().isNotFound());
    }

    public void shouldListAllFiles() throws Exception {
        // given(this.reportService.loadAsResource("test.txt")).willReturn();
        // this.mvc.perform(get("/")).andExpect(status().isOk()).andExpect(model().attribute("files",
        // Matchers.contains("http://localhost/files/first.txt",
        // "http://localhost/files/second.txt")));
    }
}
