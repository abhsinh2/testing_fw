package com.abhsinh2.testng;

import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

@ContextConfiguration(locations = { "classpath:META-INF/spring/test-service-context.xml" })
public abstract class AbstractTest extends AbstractTestNGSpringContextTests {

    @BeforeTest
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    @AfterMethod
    public void afterMethod() {

    }

    @AfterTest
    public void tearDown() {

    }
}